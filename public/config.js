window.ENV = {
  externalFormUrl: 'https://data.gov.cz/formul%C3%A1%C5%99/registrace-datov%C3%A9-sady',
  backendUrl: 'https://api-lkod.rabin.golemio.cz',
  returnUrl: 'https://api-lkod.rabin.golemio.cz/form-data',
};
