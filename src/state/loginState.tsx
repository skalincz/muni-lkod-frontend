import { Configuration, LoginInput, LoginLogoutApi, LoginOutput } from 'api';
import { config } from 'config';
import React, { createContext, FC, useCallback, useContext, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect } from 'react-router-dom';

const TOKEN_STORAGE_KEY = 'accessToken';

type LoginState = {
  accessToken: string | null;
  isLoading: boolean;
  loginError: string | null;
  isLoggedIn: boolean;
};

type LoginFc = (loginInput: LoginInput) => void;

const savedToken = localStorage.getItem(TOKEN_STORAGE_KEY);
const defaultState: LoginState = {
  accessToken: savedToken || null,
  isLoading: false,
  loginError: null,
  isLoggedIn: false,
};

const getLoginLogoutApi = (accessToken: string | null) => {
  return new LoginLogoutApi(
    new Configuration({ basePath: config.backendUrl, accessToken: accessToken || undefined })
  );
};

export const LoginContext = createContext<LoginState & { login: LoginFc; logout: () => void }>({
  ...defaultState,
  login: () => {},
  logout: () => {},
});

const getErrorKey = (reason: any) => {
  switch (reason?.status) {
    case 400:
    case 401:
      return 'errors.invalidLogin';
    default:
      return 'errors.unknown';
  }
};

export const LoginProvider: FC = ({ children }) => {
  const [accessToken, setToken] = useState<string | null>(defaultState.accessToken);
  const [isLoading, setLoading] = useState<boolean>(defaultState.isLoading);
  const [loginError, setLoginError] = useState<string | null>(defaultState.loginError);
  const { t } = useTranslation();

  const onLogin = useCallback((result: LoginOutput) => {
    localStorage.setItem(TOKEN_STORAGE_KEY, result.accessToken);
    setLoading(false);
    setToken(result.accessToken);
  }, []);

  const onError = useCallback((reason) => {
    setLoading(false);
    setLoginError(getErrorKey(reason));
  }, []);

  const login = useCallback(
    (loginInput: LoginInput) => {
      setLoginError(null);
      setLoading(true);
      getLoginLogoutApi(accessToken).login({ loginInput }).then(onLogin).catch(onError);
    },
    [accessToken, onLogin, onError]
  );

  const logout = useCallback(() => {
    getLoginLogoutApi(accessToken)
      .logout()
      .catch(() => {}); // ignore logout errors
    localStorage.removeItem(TOKEN_STORAGE_KEY);
    setToken(null);
  }, [accessToken]);

  const loginState = useMemo(() => {
    return {
      accessToken,
      isLoading,
      loginError: loginError && t(loginError),
      login,
      logout,
      isLoggedIn: !!accessToken,
    };
  }, [accessToken, isLoading, loginError, login, logout, t]);

  return <LoginContext.Provider value={loginState}>{children}</LoginContext.Provider>;
};

export const RedirectWithoutLogin: FC<{ to: string }> = ({ to, children }) => {
  const loginState = useContext(LoginContext);

  return (
    <>
      {!loginState.accessToken && <Redirect to={to} />}
      {children}
    </>
  );
};

export const RedirectWithLogin: FC<{ to: string }> = ({ to, children }) => {
  const loginState = useContext(LoginContext);

  return (
    <>
      {!!loginState.accessToken && <Redirect to={to} />}
      {children}
    </>
  );
};
