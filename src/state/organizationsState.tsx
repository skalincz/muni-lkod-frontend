import { Organization } from 'api';
import React, { createContext, FC, useCallback, useContext, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { LoginContext } from 'state/loginState';
import { ApiContext } from 'utils/api';

type OrganizationsState = {
  organizations: Organization[];
  isLoading: boolean;
  organizationsError: string | null;
};

type OrganizationsActions = {
  refreshOrganizations: () => void;
};

const defaultState: OrganizationsState = {
  organizations: [],
  isLoading: false,
  organizationsError: null,
};

export const OrganizationsContext = createContext<OrganizationsState & OrganizationsActions>({
  ...defaultState,
  refreshOrganizations: () => {},
});

export const OrganizationsProvider: FC = ({ children }) => {
  const [organizations, setOrganizations] = useState<Organization[]>(defaultState.organizations);
  const [isLoading, setLoading] = useState<boolean>(defaultState.isLoading);
  const [organizationsError, setOrganizationsError] = useState<string | null>(
    defaultState.organizationsError
  );

  const { t } = useTranslation();
  const { logout } = useContext(LoginContext);
  const { organizationsApi } = useContext(ApiContext);

  const refreshOrganizations = useCallback(() => {
    setLoading(true);
    setOrganizationsError(null);
    setOrganizations([]);
    organizationsApi
      .getAllOrganizations()
      .then((organizations) => {
        setLoading(false);
        setOrganizations(organizations);
      })
      .catch((reason) => {
        setLoading(false);
        if (reason?.status === 401) {
          logout();
          return;
        }

        setOrganizationsError('errors.unknown');
      });
  }, [organizationsApi, logout]);

  const organizationsState = useMemo(() => {
    return {
      organizations,
      isLoading,
      organizationsError: organizationsError && t(organizationsError),
      refreshOrganizations,
    };
  }, [organizations, isLoading, organizationsError, t, refreshOrganizations]);

  return (
    <OrganizationsContext.Provider value={organizationsState}>
      {children}
    </OrganizationsContext.Provider>
  );
};
