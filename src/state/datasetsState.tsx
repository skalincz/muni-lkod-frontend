import { Dataset, DatasetStatusEnum, UpdateDatasetInputStatusEnum } from 'api';
import { config } from 'config';
import React, { createContext, FC, useCallback, useContext, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { LoginContext } from 'state/loginState';
import { ApiContext } from 'utils/api';

type DatasetsState = {
  datasets: Dataset[];
  isRefreshLoading: boolean;
  isCreateLoading: boolean;
  isEditLoading: boolean;
  datasetsError: string | null;
  datasetsCreateError: string | null;
};

type DatasetsActions = {
  refreshDatasets: (organizationId?: number, statusFilter?: DatasetStatusEnum[]) => void;
  deleteDataset: (datasetId: string) => void;
  createDataset: (organizationId: number) => void;
  editDataset: (dataset: Dataset) => void;
  changePublicity: (dataset: Dataset, visible: boolean) => void;
};

const defaultState = {
  datasets: [],
  isRefreshLoading: false,
  isCreateLoading: false,
  isEditLoading: false,
  datasetsError: null,
  datasetsCreateError: null,
};

export const DatasetsContext = createContext<DatasetsState & DatasetsActions>({
  ...defaultState,
  refreshDatasets: () => {},
  editDataset: () => {},
  createDataset: () => {},
  deleteDataset: () => {},
  changePublicity: () => {},
});

export const DatasetsProvider: FC = ({ children }) => {
  const [datasets, setDatasets] = useState<Dataset[]>(defaultState.datasets);
  const [isRefreshLoading, setRefreshLoading] = useState<boolean>(defaultState.isRefreshLoading);
  const [isCreateLoading, setCreateLoading] = useState<boolean>(defaultState.isCreateLoading);
  const [isEditLoading, setEditLoading] = useState<boolean>(defaultState.isEditLoading);
  const [datasetsError, setDatasetsError] = useState<string | null>(defaultState.datasetsError);
  const [datasetsCreateError, setDatasetsCreateError] = useState<string | null>(
    defaultState.datasetsCreateError
  );
  const [organizationIdFilter, setOrganizationIdFilter] = useState<number | undefined>(undefined);
  const [statusFilter, setStatusFilter] = useState<DatasetStatusEnum[] | undefined>(undefined);

  const { datasetsApi, sessionsApi } = useContext(ApiContext);
  const { t } = useTranslation();
  const { logout, accessToken } = useContext(LoginContext);

  const onError = useCallback(
    (reason) => {
      if (reason?.status === 401) {
        logout();
        return;
      }
      setDatasetsError('errors.unknown');
    },
    [logout]
  );

  const refreshDatasets = useCallback(
    (organizationId?: number, statusFilter?: DatasetStatusEnum[]) => {
      setRefreshLoading(true);
      setDatasets([]);
      setDatasetsError(null);
      setOrganizationIdFilter(organizationId);
      setStatusFilter(statusFilter);

      datasetsApi
        .getAllDatasets({ organizationId })
        .then((datasets: Dataset[]) => {
          setDatasets(
            datasets.filter(
              (dataset) => !statusFilter?.length || statusFilter.includes(dataset.status)
            )
          );
        })
        .catch(onError)
        .finally(() => {
          setRefreshLoading(false);
        });
    },
    [datasetsApi, onError]
  );

  const goToForm = useCallback(
    async (dataset: Dataset, accessToken: string) => {
      try {
        const { id: sessionId } = await sessionsApi.createSession({
          createSessionInput: {
            datasetId: dataset.id,
          },
        });

        const form = document.createElement('form');
        form.action = `${config.externalFormUrl}?returnUrl=${encodeURI(config.returnUrl)}`;
        form.method = 'POST';

        const userInput = document.createElement('input');
        userInput.name = 'userData';
        userInput.type = 'hidden';
        userInput.value = JSON.stringify({
          accessToken,
          sessionId,
          datasetId: dataset.id,
        });
        form.appendChild(userInput);

        if (dataset.data) {
          const dataInput = document.createElement('input');
          dataInput.name = 'formData';
          dataInput.type = 'hidden';
          dataInput.value = JSON.stringify(dataset.data);

          form.appendChild(dataInput);
        }

        document.body.appendChild(form);
        form.submit();
      } catch (reason: any) {
        if (reason?.status === 401) {
          logout();
          return;
        }
        console.error(`goToForm Error: ${reason}`);
      } finally {
        setEditLoading(false);
      }
    },
    [sessionsApi, logout]
  );

  const editDataset = useCallback(
    (dataset: Dataset) => {
      if (!accessToken) {
        logout();
        return;
      }

      setEditLoading(true);
      goToForm(dataset, accessToken);
    },
    [goToForm, accessToken, logout]
  );

  const createDataset = useCallback(
    async (organizationId: number) => {
      if (!accessToken) {
        logout();
        return;
      }

      try {
        setCreateLoading(true);
        setDatasetsCreateError(null);

        const newDataset = await datasetsApi.createDataset({
          createDatasetInput: {
            organizationId,
          },
        });

        goToForm(newDataset, accessToken);
      } catch (reason: any) {
        if (reason?.status === 401) {
          logout();
          return;
        }
        setDatasetsCreateError('errors.unknown');
      } finally {
        setCreateLoading(false);
      }
    },
    [datasetsApi, logout, accessToken, goToForm]
  );

  const deleteDataset = useCallback(
    (datasetId: string) => {
      datasetsApi.deleteDataset({ datasetId }).finally(() => {
        refreshDatasets(organizationIdFilter, statusFilter);
      });
    },
    [datasetsApi, refreshDatasets, organizationIdFilter, statusFilter]
  );

  const changePublicity = useCallback(
    (dataset: Dataset, visibility: boolean) => {
      setEditLoading(true);

      datasetsApi
        .updateDataset({
          datasetId: dataset.id,
          updateDatasetInput: {
            status: visibility
              ? UpdateDatasetInputStatusEnum.Published
              : UpdateDatasetInputStatusEnum.Unpublished,
          },
        })
        .then((updatedDataset: Dataset) => {
          setDatasets(
            datasets.map((oldDataset) => {
              if (oldDataset.id === updatedDataset.id) {
                return updatedDataset;
              } else {
                return oldDataset;
              }
            })
          );
        })
        .catch(onError)
        .finally(() => {
          setEditLoading(false);
        });
    },
    [datasets, datasetsApi, onError]
  );

  const datasetsState = useMemo(() => {
    return {
      isEditLoading,
      isRefreshLoading,
      isCreateLoading,
      datasetsError: datasetsError && t(datasetsError),
      datasetsCreateError: datasetsCreateError && t(datasetsCreateError),
      datasets,
      refreshDatasets,
      editDataset,
      createDataset,
      deleteDataset,
      changePublicity,
    };
  }, [
    isEditLoading,
    isRefreshLoading,
    isCreateLoading,
    datasets,
    datasetsError,
    datasetsCreateError,
    refreshDatasets,
    t,
    editDataset,
    createDataset,
    deleteDataset,
    changePublicity,
  ]);

  return <DatasetsContext.Provider value={datasetsState}>{children}</DatasetsContext.Provider>;
};
