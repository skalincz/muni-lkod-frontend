class Config {
  public readonly externalFormUrl: string;
  public readonly backendUrl: string;
  public readonly returnUrl: string;
  public readonly publicUrl: string;
  public readonly environment: typeof process.env.NODE_ENV;

  constructor() {
    this.externalFormUrl = window.ENV.externalFormUrl;
    this.backendUrl = window.ENV.backendUrl;
    this.returnUrl = window.ENV.returnUrl;
    this.publicUrl = process.env.PUBLIC_URL || '';
    this.environment = process.env.NODE_ENV;
  }

  public isDev() {
    return this.environment === 'development';
  }
}

export const config = new Config();
