import translationCs from './translation/cs.json';

export const resources = {
  cs: { translation: translationCs },
};
