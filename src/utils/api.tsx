import {
  Configuration,
  DatasetsApi,
  FormDataApi,
  OrganizationsApi,
  SessionsApi,
  BasicApi,
} from 'api';
import { config } from 'config';
import React, { createContext, FC, useContext, useMemo } from 'react';
import { LoginContext } from 'state/loginState';

const getApiConfig = (accessToken?: string) => {
  return new Configuration({ basePath: config.backendUrl, accessToken });
};

const getApis = (accessToken?: string) => {
  const apiConfig = getApiConfig(accessToken);
  return {
    datasetsApi: new DatasetsApi(apiConfig),
    organizationsApi: new OrganizationsApi(apiConfig),
    sessionsApi: new SessionsApi(apiConfig),
    formDataApi: new FormDataApi(apiConfig),
    basicApi: new BasicApi(apiConfig),
  };
};

export const ApiContext = createContext(getApis());

export const ApiProvider: FC = ({ children }) => {
  const { accessToken } = useContext(LoginContext);

  const apis = useMemo(() => {
    return getApis(accessToken || undefined);
  }, [accessToken]);

  return <ApiContext.Provider value={apis}>{children}</ApiContext.Provider>;
};
