import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

export const useHighlightedName = () => {
  const { t } = useTranslation();

  const getHighlightedName = useCallback(
    (name?: string | null) => {
      return name ? <b>{name}</b> : <i>{t('unnamed')}</i>;
    },
    [t]
  );

  return {
    getHighlightedName,
  };
};
