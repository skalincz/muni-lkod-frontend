import { useCallback, useContext, useEffect, useMemo, useReducer } from 'react';
import { ApiContext } from 'utils/api';

interface State {
  isLoading: boolean;
  hasFailed: boolean;
  files: string[];
}

type Action =
  | { type: 'INITIAL' | 'REQUEST_DATA' | 'ERROR' }
  | { type: 'DATA_RECEIVED'; payload: string[] };

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'REQUEST_DATA':
      return { isLoading: true, hasFailed: false, files: [] };
    case 'ERROR':
      return { isLoading: false, hasFailed: true, files: [] };
    case 'DATA_RECEIVED':
      return {
        isLoading: false,
        hasFailed: false,
        files: action.payload,
      };
    case 'INITIAL':
      return { ...state };
  }
};

export const useFilesState = (datasetId: string | null) => {
  const { datasetsApi } = useContext(ApiContext);

  const [state, dispatch] = useReducer(reducer, {
    isLoading: false,
    hasFailed: false,
    files: [],
  });

  const refreshFiles = useCallback(() => {
    if (datasetId) {
      dispatch({ type: 'REQUEST_DATA' });
      datasetsApi
        .getAllFiles({ datasetId })
        .then((data) => dispatch({ type: 'DATA_RECEIVED', payload: data.files }))
        .catch(() => dispatch({ type: 'ERROR' }));
    }
  }, [datasetId, datasetsApi]);

  useEffect(() => {
    refreshFiles();
  }, [refreshFiles]);

  const ret = useMemo(() => ({ ...state, refreshFiles }), [state, refreshFiles]);

  return ret;
};
