import { useCallback, useContext, useMemo, useReducer } from 'react';
import { ApiContext } from 'utils/api';

interface State {
  isLoading: boolean;
  hasFailed: boolean;
}

type Action = { type: 'INITIAL' | 'SEND_FILE' | 'ERROR' | 'FILE_SENT' };

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'SEND_FILE':
      return { isLoading: true, hasFailed: false };
    case 'ERROR':
      return { isLoading: false, hasFailed: true };
    case 'FILE_SENT':
      return { isLoading: false, hasFailed: false };
    case 'INITIAL':
      return { ...state };
  }
};

export const useUploadFileState = (datasetId: string | null, onUpload: () => void) => {
  const { datasetsApi } = useContext(ApiContext);

  const [state, dispatch] = useReducer(reducer, {
    isLoading: false,
    hasFailed: false,
  });

  const uploadFile = useCallback(
    (file: File) => {
      if (datasetId && !state.isLoading) {
        dispatch({ type: 'SEND_FILE' });
        datasetsApi
          .uploadFile({ datasetId, file })
          .then(() => {
            dispatch({ type: 'FILE_SENT' });
            onUpload();
          })
          .catch(() => dispatch({ type: 'ERROR' }));
      }
    },
    [datasetId, state, datasetsApi, onUpload]
  );

  const ret = useMemo(() => ({ ...state, uploadFile }), [state, uploadFile]);

  return ret;
};
