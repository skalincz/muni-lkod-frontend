declare interface Window {
  ENV: {
    externalFormUrl: string;
    backendUrl: string;
    returnUrl: string;
  };
}
