import React, { FC } from 'react';
import './config'; // explicitly init config
import './i18n';
import 'moment/locale/cs';
import moment from 'moment-timezone';
import { ThemeProvider } from '@material-ui/core';
import { theme } from 'utils/theme';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { config } from './config';
import { routerPaths } from 'utils/routerPaths';
import { HomePage } from 'pages/homePage';
import { LoginPage } from 'pages/loginPage';
import { LoginProvider, RedirectWithLogin, RedirectWithoutLogin } from 'state/loginState';
import { ApiProvider } from 'utils/api';
import { DatasetsProvider } from 'state/datasetsState';
import { OrganizationsProvider } from 'state/organizationsState';

moment.locale('cs');

const Providers: FC = ({ children }) => {
  return (
    <LoginProvider>
      <ApiProvider>
        <DatasetsProvider>
          <OrganizationsProvider>
            <ThemeProvider theme={theme}>{children}</ThemeProvider>
          </OrganizationsProvider>
        </DatasetsProvider>
      </ApiProvider>
    </LoginProvider>
  );
};

function App() {
  return (
    <Providers>
      <BrowserRouter basename={config.publicUrl}>
        <Switch>
          <Route path={routerPaths.home} exact={true}>
            <RedirectWithoutLogin to={routerPaths.login}>
              <HomePage />
            </RedirectWithoutLogin>
          </Route>
          <Route path={routerPaths.login} exact={true}>
            <RedirectWithLogin to={routerPaths.home}>
              <LoginPage />
            </RedirectWithLogin>
          </Route>
          <Redirect to={routerPaths.home} />
        </Switch>
      </BrowserRouter>
    </Providers>
  );
}

export default App;
