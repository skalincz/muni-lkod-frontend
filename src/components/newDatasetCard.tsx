import {
  Box,
  Button,
  Card,
  CardContent,
  CircularProgress,
  FormLabel,
  makeStyles,
  MenuItem,
  Select,
  Typography,
} from '@material-ui/core';
import React, { FC, useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DatasetsContext } from 'state/datasetsState';
import { OrganizationsContext } from 'state/organizationsState';
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles((theme) => ({
  newDatasetSelect: {
    margin: '0 8px',
  },
  newDatasetButton: {
    margin: 8,
  },
}));

export const NewDatasetCard: FC<{ className?: string }> = ({ className }) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const {
    organizations,
    isLoading: isOrganizationsLoading,
    organizationsError,
    refreshOrganizations,
  } = useContext(OrganizationsContext);
  const { createDataset, isCreateLoading, datasetsCreateError } = useContext(DatasetsContext);

  const [selectedOrganization, selectOranization] = useState<number | null>(null);

  useEffect(
    () => {
      refreshOrganizations();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <Card className={className}>
      <CardContent>
        <Box textAlign="center">
          <Typography variant="h2">{t('newDatasetTitle')}</Typography>
          {(isOrganizationsLoading || !!organizationsError) && <CircularProgress />}
          {!!organizationsError && <Typography color="error">{organizationsError}</Typography>}
          {!isOrganizationsLoading && organizations.length === 0 && (
            <Typography color="error">{t('errors.noOrganization')}</Typography>
          )}
          {organizations.length > 0 && (
            <>
              <div>
                <FormLabel>{t('organization')}:</FormLabel>
                <Select
                  className={classes.newDatasetSelect}
                  variant="standard"
                  defaultValue={organizations[0].id}
                  onChange={(e) => selectOranization(e.target.value as number)}
                  disabled={isCreateLoading}
                >
                  {organizations.map((organization) => (
                    <MenuItem key={organization.id} value={organization.id}>
                      {organization.name}
                    </MenuItem>
                  ))}
                </Select>
                <Button
                  className={classes.newDatasetButton}
                  color="primary"
                  variant="contained"
                  type="button"
                  disabled={isCreateLoading}
                  onClick={() => createDataset(selectedOrganization ?? organizations[0].id)}
                  title={t('createDataset')}
                >
                  <AddIcon />
                </Button>
              </div>
              {!!datasetsCreateError && (
                <div>
                  <Typography>{datasetsCreateError}</Typography>
                </div>
              )}
            </>
          )}
        </Box>
      </CardContent>
    </Card>
  );
};
