import { Button, CircularProgress, Typography } from '@material-ui/core';
import React, { FC, useContext, useState } from 'react';
import DeleteIcon from '@material-ui/icons/Delete';
import { ApiContext } from 'utils/api';
import { useTranslation } from 'react-i18next';

interface Props {
  datasetId: string;
  filename: string;
}

export const DeleteDatasetFileButton: FC<Props> = ({ datasetId, filename }) => {
  const [isLoading, setLoading] = useState(false);
  const [isDeleted, setDeleted] = useState(false);
  const { t } = useTranslation();
  const { datasetsApi } = useContext(ApiContext);

  const onClick = () => {
    if (isLoading) {
      return;
    }

    setLoading(true);
    datasetsApi
      .deleteFile({ datasetId, filename })
      .then(() => setDeleted(true))
      .finally(() => setLoading(false));
  };

  if (isDeleted) {
    return <Typography color="textSecondary">{t('deleted')}</Typography>;
  }

  return (
    <Button disabled={isLoading} variant="contained" color="secondary" onClick={onClick}>
      {isLoading ? <CircularProgress size={24} /> : <DeleteIcon />}
    </Button>
  );
};
