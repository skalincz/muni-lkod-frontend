import {
  Chip,
  CircularProgress,
  FormLabel,
  makeStyles,
  MenuItem,
  Paper,
  Select,
  Typography,
} from '@material-ui/core';
import { DatasetStatusEnum } from 'api';
import React, { Dispatch, FC, SetStateAction, useCallback, useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { OrganizationsContext } from 'state/organizationsState';

const useStyles = makeStyles((theme) => ({
  container: {
    width: '100%',
    display: 'flex',
    marginTop: theme.spacing(1),
    padding: theme.spacing(1),
    justifyContent: 'space-between',
    alignItems: 'baseline',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 4,
  },
}));

const Filters: FC<{
  organizationFilter: number | undefined;
  setOrganizationFilter: Dispatch<SetStateAction<number | undefined>>;
  statusFilter: DatasetStatusEnum[];
  setStatusFilter: Dispatch<SetStateAction<DatasetStatusEnum[]>>;
}> = ({ organizationFilter, setOrganizationFilter, statusFilter, setStatusFilter }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const { organizations, isLoading } = useContext(OrganizationsContext);

  const handleStatusFilterChange = useCallback(
    (status: DatasetStatusEnum) => {
      const isActive = statusFilter.includes(status);

      const newFilter = isActive
        ? statusFilter.filter((filterStatus) => filterStatus !== status)
        : [...statusFilter, status];

      setStatusFilter(newFilter);
    },
    [statusFilter, setStatusFilter]
  );

  return (
    <Paper elevation={2} className={classes.container}>
      <div>
        <Typography>{t('filters')}: </Typography>
      </div>
      <div>
        <FormLabel>{t('organization')}: </FormLabel>
        {isLoading && <CircularProgress />}
        {!isLoading && (
          <Select
            value={organizationFilter ?? 'any'}
            onChange={(e) => {
              const value = e.target.value as number | 'any';
              const id = value === 'any' ? undefined : value;
              setOrganizationFilter(id);
            }}
          >
            <MenuItem value="any">{t('any')}</MenuItem>
            {organizations.map((organization) => (
              <MenuItem key={organization.id} value={organization.id}>
                {organization.name}
              </MenuItem>
            ))}
          </Select>
        )}
      </div>
      <div>
        <FormLabel>{t('status')}: </FormLabel>
        {Object.values(DatasetStatusEnum).map((status) => (
          <Chip
            onClick={() => handleStatusFilterChange(status)}
            className={classes.chip}
            key={status}
            label={t(`statuses.${status}`)}
            color={statusFilter.includes(status) ? 'primary' : 'default'}
          />
        ))}
      </div>
    </Paper>
  );
};

export const useFilters = () => {
  const [organizationFilter, setOrganizationFilter] = useState<number | undefined>(undefined);
  const [statusFilter, setStatusFilter] = useState<DatasetStatusEnum[]>(
    Object.values(DatasetStatusEnum)
  );

  return {
    Filters: () => (
      <Filters
        organizationFilter={organizationFilter}
        setOrganizationFilter={setOrganizationFilter}
        statusFilter={statusFilter}
        setStatusFilter={setStatusFilter}
      />
    ),
    organizationFilter,
    statusFilter,
  };
};
