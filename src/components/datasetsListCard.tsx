import {
  Button,
  Card,
  CardContent,
  CircularProgress,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { Dataset, DatasetStatusEnum } from 'api';
import React, { FC, useCallback, useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DatasetsContext } from 'state/datasetsState';
import { useHighlightedName } from 'utils/getHighlightedName';
import moment from 'moment-timezone';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import FolderOpenIcon from '@material-ui/icons/FolderOpen';
import { DeleteDialog } from 'components/deleteDialog';
import { useFilters } from 'components/filters';
import { FilesDialog } from './filesDialog';

const TIME_FORMAT = 'DD. MM. YYYY HH:mm';

const useStyles = makeStyles((theme) => ({
  tableScroll: {
    overflowX: 'auto',
  },
  tableButton: {
    minWidth: 32,

    '&:not(last-child)': {
      marginRight: theme.spacing(2),
    },
  },
  buttonHolder: {
    display: 'flex',
  },
}));

export const DatasetsListCard: FC<{ className?: string }> = ({ className }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const { getHighlightedName } = useHighlightedName();
  const { Filters, organizationFilter, statusFilter } = useFilters();

  const {
    datasets,
    isRefreshLoading,
    refreshDatasets,
    datasetsError,
    isEditLoading,
    editDataset,
    changePublicity,
  } = useContext(DatasetsContext);

  const [datasetToDelete, setDatasetToDelete] = useState<Dataset | null>(null);
  const [detailDataset, setDetailDataset] = useState<Dataset | null>(null);
  const [isDeleteConfirmOpen, setDeleteConfirmOpen] = useState(false);

  const showDeleteConfirmation = useCallback((dataset: Dataset) => {
    setDatasetToDelete(dataset);
    setDeleteConfirmOpen(true);
  }, []);

  const closeDialog = useCallback(() => {
    setDeleteConfirmOpen(false);
  }, []);

  useEffect(
    () => {
      refreshDatasets(organizationFilter, statusFilter);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [organizationFilter, statusFilter]
  );

  return (
    <Card className={className}>
      <CardContent>
        <Typography align="center" variant="h2">
          {t('datasetsTitle')}
        </Typography>
        <Filters />
        <div className={classes.tableScroll}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{t('name')}</TableCell>
                <TableCell>{t('status')}</TableCell>
                <TableCell>{t('organization')}</TableCell>
                <TableCell>{t('createdAt')}</TableCell>
                <TableCell>{t('updatedAt')}</TableCell>
                <TableCell>{t('actions')}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {!isRefreshLoading && !datasetsError && datasets.length === 0 && (
                <TableRow>
                  <TableCell align="center" colSpan={6}>
                    <Typography>{t('noResultsForChosenFilter')}</Typography>
                  </TableCell>
                </TableRow>
              )}
              {(isRefreshLoading || !!datasetsError) && (
                <TableRow>
                  <TableCell align="center" colSpan={6}>
                    {isRefreshLoading && <CircularProgress />}
                    {!!datasetsError && <Typography color="error">{datasetsError}</Typography>}
                  </TableCell>
                </TableRow>
              )}
              {datasets.map((dataset: Dataset) => (
                <TableRow key={dataset.id}>
                  <TableCell>{getHighlightedName(dataset?.name)}</TableCell>
                  <TableCell>{t(`statuses.${dataset.status}`)}</TableCell>
                  <TableCell>{dataset.organization.name}</TableCell>
                  <TableCell>{moment(dataset.createdAt).format(TIME_FORMAT)}</TableCell>
                  <TableCell align="center">
                    {dataset.updatedAt ? (
                      moment(dataset.updatedAt).format(TIME_FORMAT)
                    ) : (
                      <i>{t('noChanges')}</i>
                    )}
                  </TableCell>
                  <TableCell size="small">
                    <div className={classes.buttonHolder}>
                      <Button
                        className={classes.tableButton}
                        variant="contained"
                        type="button"
                        size="small"
                        disabled={isEditLoading}
                        onClick={() => setDetailDataset(dataset)}
                        title={t('files')}
                      >
                        <FolderOpenIcon />
                      </Button>
                      <Button
                        className={classes.tableButton}
                        variant="contained"
                        type="button"
                        size="small"
                        disabled={isEditLoading}
                        onClick={() => {
                          editDataset(dataset);
                        }}
                        title={t('edit')}
                      >
                        <EditIcon />
                      </Button>
                      <Button
                        className={classes.tableButton}
                        variant="contained"
                        type="button"
                        size="small"
                        disabled={dataset.status === DatasetStatusEnum.Created || isEditLoading}
                        onClick={() => {
                          changePublicity(dataset, dataset.status !== DatasetStatusEnum.Published);
                        }}
                        title={
                          dataset.status === DatasetStatusEnum.Published
                            ? t('unpublish')
                            : t('publish')
                        }
                      >
                        {dataset.status === DatasetStatusEnum.Published ? (
                          <VisibilityOffIcon />
                        ) : (
                          <VisibilityIcon />
                        )}
                      </Button>
                      <Button
                        className={classes.tableButton}
                        variant="contained"
                        type="button"
                        size="small"
                        color="secondary"
                        onClick={() => {
                          showDeleteConfirmation(dataset);
                        }}
                        title={t('delete')}
                      >
                        <DeleteIcon />
                      </Button>
                    </div>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      </CardContent>
      <FilesDialog
        closeDialog={() => setDetailDataset(null)}
        dataset={detailDataset}
        isOpen={!!detailDataset}
      />
      <DeleteDialog
        closeDialog={closeDialog}
        datasetToDelete={datasetToDelete}
        isOpen={isDeleteConfirmOpen}
      />
    </Card>
  );
};
