import React, { FC, useMemo } from 'react';
import {
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Link,
  List,
  ListItem,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { Dataset } from 'api';
import { Trans, useTranslation } from 'react-i18next';
import AttachFile from '@material-ui/icons/AttachFile';
import { useFilesState } from 'hooks/useFilesState';
import { useUploadFileState } from 'hooks/useUploadFileState';
import { DeleteDatasetFileButton } from './deleteDatasetFileButton';

const createStyles = makeStyles(() => ({
  loaderContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  fileListItem: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));

export const FilesDialog: FC<{
  isOpen: boolean;
  closeDialog: () => void;
  dataset: Dataset | null;
}> = ({ isOpen, closeDialog, dataset }) => {
  const { t } = useTranslation();
  const datasetId = dataset?.id || null;
  const {
    files,
    isLoading: areFilesLoading,
    hasFailed: haveFilesFailed,
    refreshFiles,
  } = useFilesState(datasetId);
  const { isLoading: isUploadLoading, uploadFile } = useUploadFileState(datasetId, refreshFiles);

  const isLoading = isUploadLoading || areFilesLoading;

  const transformedFiles = useMemo(
    () =>
      files.map((file) => {
        const split = file.split('/');
        const filename = split[split.length - 1];

        return { name: filename, url: file };
      }),
    [files]
  );

  const onFileInputChange = (inputFiles: FileList | null) => {
    if (!inputFiles?.length) {
      return;
    }

    const file = inputFiles[0];
    uploadFile(file);
  };

  const classes = createStyles();

  return (
    <Dialog open={isOpen} onClose={closeDialog}>
      {!!dataset && !!datasetId && (
        <>
          <DialogTitle>
            <Trans
              i18nKey="filesOf"
              components={[<b></b>]}
              values={{ datasetName: dataset.name }}
            ></Trans>
          </DialogTitle>
          <DialogContent>
            {isLoading && (
              <div className={classes.loaderContainer}>
                <CircularProgress />
              </div>
            )}
            {haveFilesFailed && (
              <Typography color="secondary">{t('errors.filesDownloadFailed')}</Typography>
            )}
            {!isLoading && !haveFilesFailed && (
              <List>
                {transformedFiles.map((file) => (
                  <ListItem className={classes.fileListItem} key={file.url}>
                    <Link href={file.url} download={true} target="_blank">
                      {file.name}
                    </Link>
                    <DeleteDatasetFileButton datasetId={datasetId} filename={file.name} />
                  </ListItem>
                ))}
                {files.length === 0 && <DialogContentText>{t('noFiles')}</DialogContentText>}
              </List>
            )}
          </DialogContent>
          <DialogActions>
            <Button variant="contained" onClick={closeDialog}>
              {t('close')}
            </Button>
            <Button
              variant="contained"
              color="primary"
              startIcon={<AttachFile />}
              component="label"
              disabled={isLoading || haveFilesFailed}
            >
              <input
                type="file"
                hidden={true}
                onChange={(e) => onFileInputChange(e.currentTarget.files)}
              />
              {t('addFile')}
            </Button>
          </DialogActions>
        </>
      )}
    </Dialog>
  );
};
