import { makeStyles, Paper, Typography } from '@material-ui/core';
import React, { FC } from 'react';

const useStyles = makeStyles({
  footer: {
    padding: 15,
    textAlign: 'center',
  },
});

export const Footer: FC = () => {
  const classes = useStyles();

  return (
    <Paper square={true} className={classes.footer} elevation={3}>
      <Typography>Operátor ICT, 2020</Typography>
    </Paper>
  );
};
