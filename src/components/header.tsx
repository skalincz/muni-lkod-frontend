import { Button, makeStyles, Paper, Typography } from '@material-ui/core';
import React, { FC, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { LoginContext } from 'state/loginState';
import { routerPaths } from 'utils/routerPaths';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.primary.main,
    display: 'flex',
    justifyContent: 'center',
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: 15,
    color: theme.palette.common.white,
    flexBasis: theme.breakpoints.values.md,
  },
  homeLink: {
    color: 'inherit',
    textDecoration: 'none',
  },
}));

export const Header: FC = () => {
  const { t } = useTranslation();
  const classes = useStyles();
  const { isLoggedIn, logout } = useContext(LoginContext);

  return (
    <Paper square={true} elevation={3} className={classes.container}>
      <div className={classes.header}>
        <Typography variant="h1">
          <Link className={classes.homeLink} to={routerPaths.home}>
            {t('title')}
          </Link>
        </Typography>
        {isLoggedIn && (
          <Button type="button" onClick={logout} variant="contained" title={t('logout')}>
            <ExitToAppIcon />
          </Button>
        )}
      </div>
    </Paper>
  );
};
