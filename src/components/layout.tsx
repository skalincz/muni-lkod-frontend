import { makeStyles } from '@material-ui/core';
import { Footer } from 'components/footer';
import { Header } from 'components/header';
import React, { FC } from 'react';

const useStyles = makeStyles({
  layout: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
  },
  content: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },
});

export const Layout: FC = ({ children }) => {
  const classes = useStyles();

  return (
    <div className={classes.layout}>
      <Header />
      <div className={classes.content}>{children}</div>
      <Footer />
    </div>
  );
};
