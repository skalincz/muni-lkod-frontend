import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';
import { Dataset } from 'api';
import React, { FC, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { DatasetsContext } from 'state/datasetsState';
import { useHighlightedName } from 'utils/getHighlightedName';
import DeleteIcon from '@material-ui/icons/Delete';
import CancelIcon from '@material-ui/icons/Cancel';

export const DeleteDialog: FC<{
  isOpen: boolean;
  datasetToDelete: Dataset | null;
  closeDialog: () => void;
}> = ({ isOpen, datasetToDelete, closeDialog }) => {
  const { t } = useTranslation();
  const { getHighlightedName } = useHighlightedName();
  const { deleteDataset } = useContext(DatasetsContext);

  return (
    <Dialog open={isOpen && !!datasetToDelete} onClose={closeDialog}>
      {datasetToDelete && (
        <>
          <DialogTitle>{t('deleteConfirmationTitle')}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {t('deleteConfirmationText1')}
              {getHighlightedName(datasetToDelete.name)}
              {t('deleteConfirmationText2')}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button variant="contained" onClick={closeDialog}>
              <CancelIcon />
              {t('cancel')}
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => {
                closeDialog();
                deleteDataset(datasetToDelete.id);
              }}
            >
              <DeleteIcon />
              {t('delete')}
            </Button>
          </DialogActions>
        </>
      )}
    </Dialog>
  );
};
