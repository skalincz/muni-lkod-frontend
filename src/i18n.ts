import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { resources } from 'locales';
import { config } from 'config';

i18n.use(initReactI18next).init({
  resources,
  lng: 'cs',
  fallbackLng: 'cs',
  debug: config.isDev(),
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
