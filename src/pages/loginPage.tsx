import { Button, makeStyles, Paper, TextField, Typography } from '@material-ui/core';
import { LoginInput } from 'api';
import { Layout } from 'components/layout';
import { Formik } from 'formik';
import React, { FC, useCallback, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { LoginContext } from 'state/loginState';
import * as Yup from 'yup';

const useStyles = makeStyles({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    padding: 20,
  },
  loginBox: {
    maxWidth: 500,
    padding: '20px 30px',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    '& > *': {
      margin: 8,
    },
  },
});

export const LoginPage: FC = () => {
  const classes = useStyles();
  const { t } = useTranslation();
  const { login, isLoading, loginError } = useContext(LoginContext);
  const onSubmit = useCallback(
    (loginInput: LoginInput) => {
      login(loginInput);
    },
    [login]
  );

  return (
    <Layout>
      <div className={classes.container}>
        <Paper className={classes.loginBox} elevation={3}>
          <Formik
            initialValues={{ email: '', password: '' }}
            onSubmit={onSubmit}
            validateOnBlur={true}
            validateOnChange={false}
            validationSchema={Yup.object().shape({
              email: Yup.string().email(t('errors.notAnEmail')).required(t('errors.required')),
              password: Yup.string().required(t('errors.required')),
            })}
          >
            {({ handleSubmit, handleBlur, handleChange, errors, touched }) => (
              <form noValidate={true} className={classes.form} onSubmit={handleSubmit}>
                <h2>{t('login')}</h2>
                <TextField
                  name="email"
                  type="email"
                  label={t('email')}
                  required={true}
                  variant="outlined"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={touched.email && !!errors.email}
                  helperText={touched.email && errors.email}
                />
                <TextField
                  name="password"
                  type="password"
                  label={t('password')}
                  required={true}
                  variant="outlined"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={touched.password && !!errors.password}
                  helperText={touched.password && errors.password}
                />
                <Button disabled={isLoading} variant="contained" color="primary" type="submit">
                  {t('doLogin')}
                </Button>
                <Typography color="error">{loginError}</Typography>
              </form>
            )}
          </Formik>
        </Paper>
      </div>
    </Layout>
  );
};
