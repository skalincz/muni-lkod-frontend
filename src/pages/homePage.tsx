import { makeStyles } from '@material-ui/core';
import { Layout } from 'components/layout';
import React, { FC } from 'react';
import { NewDatasetCard } from 'components/newDatasetCard';
import { DatasetsListCard } from 'components/datasetsListCard';

const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: theme.breakpoints.values.md,
    width: '100%',
    margin: '0 auto',

    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),

    [theme.breakpoints.down('xs')]: {
      paddingLeft: theme.spacing(0),
      paddingRight: theme.spacing(0),
    },
  },
  card: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),

    [theme.breakpoints.down('sm')]: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
    },
  },
}));

export const HomePage: FC = () => {
  const classes = useStyles();

  return (
    <Layout>
      <div className={classes.container}>
        <NewDatasetCard className={classes.card} />
        <DatasetsListCard className={classes.card} />
      </div>
    </Layout>
  );
};
