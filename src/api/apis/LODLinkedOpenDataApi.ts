/* tslint:disable */
/* eslint-disable */
/**
 * Golemio LKOD API
 * API for Local catalog of open data (LKOD)
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import {
    LODCatalog,
    LODCatalogFromJSON,
    LODCatalogToJSON,
    LODDataset,
    LODDatasetFromJSON,
    LODDatasetToJSON,
} from '../models';

export interface GetLODDatasetRequest {
    datasetId: string;
}

/**
 * 
 */
export class LODLinkedOpenDataApi extends runtime.BaseAPI {

    /**
     * LOD Catalog definition
     */
    async getLODCatalogRaw(): Promise<runtime.ApiResponse<LODCatalog>> {
        const queryParameters: runtime.HTTPQuery = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/lod/catalog`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        });

        return new runtime.JSONApiResponse(response, (jsonValue) => LODCatalogFromJSON(jsonValue));
    }

    /**
     * LOD Catalog definition
     */
    async getLODCatalog(): Promise<LODCatalog> {
        const response = await this.getLODCatalogRaw();
        return await response.value();
    }

    /**
     * LOD Dataset
     */
    async getLODDatasetRaw(requestParameters: GetLODDatasetRequest): Promise<runtime.ApiResponse<LODDataset>> {
        if (requestParameters.datasetId === null || requestParameters.datasetId === undefined) {
            throw new runtime.RequiredError('datasetId','Required parameter requestParameters.datasetId was null or undefined when calling getLODDataset.');
        }

        const queryParameters: runtime.HTTPQuery = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/lod/catalog/{datasetId}`.replace(`{${"datasetId"}}`, encodeURIComponent(String(requestParameters.datasetId))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        });

        return new runtime.JSONApiResponse(response, (jsonValue) => LODDatasetFromJSON(jsonValue));
    }

    /**
     * LOD Dataset
     */
    async getLODDataset(requestParameters: GetLODDatasetRequest): Promise<LODDataset> {
        const response = await this.getLODDatasetRaw(requestParameters);
        return await response.value();
    }

}
