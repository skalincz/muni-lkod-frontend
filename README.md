# LKOD test frontend

Basic web page to test [LKOD Backend](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend).

## Prerequisites

- Web server, node

## Configuration

### Before build

Copy the `.env.example` file to `.env` and set all environmental variables in the new `.env` file, or set environmental variables directly.

### After build

Edit file `build/config.js` with desired values.

## Run locally

- `yarn start`

## Build and run

- `yarn build`
- serve `/build` folder with your web server

## Running on url subpath
For running frontend on url subpath like `mydomain.com/my-super-lkod` update your `.env` file to
```
PUBLIC_URL=/my-super-lkod
```
and build your own docker image.


## TODO

- better error handling (logout on 401, notification about expired token, better code reuse, ...)

## Troubleshooting

Contact vyvoj@operatorict.cz
